package domain;

public enum Region {
    AARHUS, COPENHAGEN, ODENSE, AALBORG
}
