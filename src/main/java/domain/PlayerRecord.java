package domain;

public class PlayerRecord {

    private String playerID;
    private String playerName;
    private String groupName;
    private Region region;

    private String positionAsString;
    private String accessToken;

    public String getPlayerId() {
        return playerID;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }

    public Object getPositionAsString() {
        return positionAsString;
    }

    public void setPositionAsString(String positionAsString) {
        this.positionAsString = positionAsString;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getAccessToken(){
        return this.accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public boolean isInCave() {
        return accessToken != null;
    }
}
