package domain;

import service.PlayerService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ServiceState {
    private String state = "healthy";
    private String title = "PlayerService";
    private String version = "v1";
    private String database = "fake";
    private String started;

    public ServiceState(PlayerService service, String version){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        this.started = dtf.format(LocalDateTime.now());
        this.database = service.CheckState();
        this.version = version;
    }
}
