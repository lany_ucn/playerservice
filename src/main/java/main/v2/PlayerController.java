package main.v2;

import com.google.gson.Gson;
import domain.PlayerRecord;
import domain.ServiceState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.PlayerService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static spark.Spark.*;

public class PlayerController {
    private static final String version = "v2";
    private static final String baseResource = "/msdo/" + version + "/players";
    private static final Logger logger = LoggerFactory.getLogger("Main");

    public static void Start(PlayerService service) {

        Gson gson = new Gson();
        ServiceState check = new ServiceState(service, version);


        get(baseResource + "/health", (req, res) -> {
            logger.info("Method=GET " + baseResource + "/health");
            res.type("application/json");
            return gson.toJson(check);
        });

        // get player by id
        get(baseResource + "/:playerId", (req, res) -> {

            String playerId = req.params(":playerId");
            logger.info("Method=GET " + baseResource + "/" + playerId);
            PlayerRecord result = service.getPlayerById(playerId);

            res.status(result == null ? HttpServletResponse.SC_NOT_FOUND : HttpServletResponse.SC_OK);
            res.type("application/json");

            String jsonResult = gson.toJson(result);

            logger.info("Method=GET, playerId=" + playerId + ", response=" + jsonResult);
            return jsonResult;
        });

        // compute list of players at position
        get(baseResource + "/room/:positionString", (req, res) -> {
            String positionString = req.params(":positionString");
            logger.info("Method=GET " + baseResource + "/room/" + positionString );
            List<PlayerRecord> result = service.computeListOfPlayersAt(positionString);
            res.type("application/json");
            if (result.isEmpty())
                res.status(HttpServletResponse.SC_NOT_FOUND);
            return gson.toJson(result);
        });

        // update player
        post(baseResource, (req, res) -> {
            // getting request data
            String playerJson = req.body();
            PlayerRecord player = gson.fromJson(playerJson, PlayerRecord.class);
            logger.info("Method=POST "+ baseResource +", playerData=" + playerJson);

            // Check input
            if (checkPlayerModel(player)) {
                // check if the player exists
                PlayerRecord playerRecord = service.getPlayerById(player.getPlayerId());
                res.type("application/json");
                if (playerRecord == null) {
                    logger.info("Creating player record, playerData=" + playerJson);
                    String result = service.updatePlayerRecord(player);
                    res.header("Location", baseResource + "/" + player.getPlayerId());
                    res.status(result.equals("CREATED") ? HttpServletResponse.SC_CREATED
                            : HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                } else {
                    logger.info("Player record already exists, playerData=" + playerJson);
                    res.status(HttpServletResponse.SC_FORBIDDEN);
                }
            } else {
                res.status(HttpServletResponse.SC_BAD_REQUEST);
            }
            logger.info("Sending response v2 ("+ res.status() +")");
            return gson.toJson(player);
        });

        put(baseResource, (req, res) -> {
            // getting request data
            String playerJson = req.body();
            PlayerRecord player = gson.fromJson(playerJson, PlayerRecord.class);
            logger.info("Method=PUT "+ baseResource +", playerData=" + playerJson);

            if (checkPlayerModel(player)) {
                // check if the player exists
                PlayerRecord playerRecord = service.getPlayerById(player.getPlayerId());
                res.type("application/json");
                if (playerRecord != null) {
                    String result = service.updatePlayerRecord(player);
                    res.header("Location", baseResource + player.getPlayerId());
                    res.status(result.equals("UPDATED") ? HttpServletResponse.SC_OK
                            : HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                } else {
                    res.status(HttpServletResponse.SC_NOT_FOUND);
                }
            } else {
                res.status(HttpServletResponse.SC_BAD_REQUEST);
            }
            logger.info("Sending response v2 ("+ res.status() +")");
            return gson.toJson(player);
        });
    }

    private static boolean checkPlayerModel(PlayerRecord player) {
        return player.getPlayerId() != null &&
                player.getPlayerName() != null &&
                player.getGroupName() != null &&
                player.getRegion() != null &&
                player.getPositionAsString() != null;
    }
}
