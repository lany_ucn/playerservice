package main;

import com.google.gson.Gson;
import domain.PlayerRecord;
import domain.ServiceState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.FakePlayerService;
import server.RedisPlayerService;
import service.PlayerService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static spark.Spark.port;
import static spark.Spark.get;
import static spark.Spark.post;


public class Main {

    private static final Logger logger = LoggerFactory.getLogger("Main");

    public static void main(String[] args) {

        PlayerService service;

        if(args.length > 0 && args[0].equals("redis")){
            service = new RedisPlayerService("redis", 6379);
            logger.info("Redis PLayerService created");
        }
        else {
            service = new FakePlayerService();
            logger.info("Fake PLayerService created");
        }

        // defining listening port
        port(39021); 

        // adding endpoints
        main.v1.PlayerController.Start(service);
        main.v2.PlayerController.Start(service);
    }
}
