package main.v1;

import com.google.gson.Gson;
import domain.PlayerRecord;
import domain.ServiceState;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.slf4j.LoggerFactory;
import service.PlayerService;
import org.slf4j.Logger;

import com.google.gson.reflect.TypeToken;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static spark.Spark.*;


public class PlayerController {

    private static final String version = "v1";
    private static final String baseResource = "/msdo/" + version + "/players";
    private static final Logger logger = LoggerFactory.getLogger("Main");

    public static void Start(PlayerService service) {

        Gson gson = new Gson();
        ServiceState check = new ServiceState(service, version);

        get(baseResource + "/health", (req, res) -> {
            logger.info("Method=GET " + baseResource + "/health");
            res.type("application/json");
            return gson.toJson(check);
        });

        // get player by id
        get(baseResource + "/:playerId", (req, res) -> {
            String playerId = req.params(":playerId");
            logger.info("Method=GET " + baseResource + "/" + playerId);
            logger.info("Redirecting to v2");
            // calling v2
            HttpResponse<JsonNode> response =
                    Unirest.get("http://127.0.0.1:" + port() + "/" + getVersion2BaseUrl() + "/{playerId}")
                            .routeParam("playerId", playerId)
                            .asJson();

            PlayerRecord result = gson.fromJson(String.valueOf(response.getBody()), PlayerRecord.class);

            res.status(response.getStatus());
            res.type("application/json");
            String jsonResult = gson.toJson(result);
            logger.info("Method=GET, playerId=" + playerId + ", response=" + jsonResult);
            return jsonResult;
        });

        // compute list of players at position
        get(baseResource + "/room/:positionString", (req, res) -> {
            String positionString = req.params(":positionString");
            logger.info("Method=GET "+ baseResource + "/room/"+ positionString);

            logger.info("Redirecting to v2");
            // calling v2
            HttpResponse<JsonNode> response =
                    Unirest.get("http://127.0.0.1:" + port() + "/" + getVersion2BaseUrl() + "/room/{positionString}")
                            .routeParam("positionString", positionString)
                            .asJson();

            Type listType = new TypeToken<ArrayList<PlayerRecord>>() {
            }.getType();

            List<PlayerRecord> result = gson.fromJson(String.valueOf(response.getBody()), listType);

            res.type("application/json");
            if (result.isEmpty())
                res.status(HttpServletResponse.SC_NOT_FOUND);
            return gson.toJson(result);
        });

        // update player
        post(baseResource + "/:playerId", (req, res) -> {
            // getting request data
            String playerId = req.params(":playerId");
            String playerJson = req.body();
            logger.info("Method=POST "+ baseResource + "/"+ playerId +", playerData=" + playerJson);
            PlayerRecord player = gson.fromJson(playerJson, PlayerRecord.class);

            // Check input
            if (checkPlayerModel(player)) {

                HttpResponse<JsonNode> response;

                // if the player record already exists, call the update endpoint on the new version
                if (service.getPlayerById(playerId) != null) {
                    logger.info("Redirecting to PUT on v2");
                    response = Unirest.put("http://127.0.0.1:" + port() + "/" + getVersion2BaseUrl())
                            .body(playerJson)
                            .asJson();
                } else { // otherwise, call the create endpoint
                    logger.info("Redirecting to POST on v2");
                    response = Unirest.post("http://127.0.0.1:" + port() + "/" + getVersion2BaseUrl())
                            .body(playerJson)
                            .asJson();
                    res.header("Location", "/msdo/v1/players/" + playerId);
                }
                res.type("application/json");
                res.status(response.getStatus());
            } else {
                res.status(HttpServletResponse.SC_BAD_REQUEST);
            }
            logger.info("Sending response v1 ("+ res.status() +")");
            return gson.toJson(player);
        });

    }

    private static String getVersion2BaseUrl() {
        return baseResource.replaceAll("v1", "v2");
    }

    private static boolean checkPlayerModel(PlayerRecord player) {
        return player.getPlayerId() != null &&
                player.getPlayerName() != null &&
                player.getGroupName() != null &&
                player.getRegion() != null &&
                player.getPositionAsString() != null;
    }
}
