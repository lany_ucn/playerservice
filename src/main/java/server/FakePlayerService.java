package server;

import domain.Region;
import service.PlayerService;
import domain.PlayerRecord;

import java.util.*;

public class FakePlayerService implements PlayerService {

    Map<String, PlayerRecord> playerId2PlayerSpecs;

    public FakePlayerService() {
        playerId2PlayerSpecs = new HashMap<>(5);
        PlayerRecord testPlayer = new PlayerRecord();

        String playerId = "testplayer";
        testPlayer.setPlayerID(playerId);
        testPlayer.setPlayerName("Test Player");
        testPlayer.setRegion(Region.AARHUS);
        testPlayer.setGroupName("Golf");
        testPlayer.setPositionAsString("(0,1,0)");
        testPlayer.setAccessToken("khjsdfkjghksdbgk");

        playerId2PlayerSpecs.put(playerId, testPlayer);
    }

    @Override
    public PlayerRecord getPlayerById(String playerId) {
        return playerId2PlayerSpecs.get(playerId);
    }

    @Override
    public String updatePlayerRecord(PlayerRecord player) {
        PlayerRecord old = playerId2PlayerSpecs.put(player.getPlayerId(), player);
        return old == null ? "CREATED" : "UPDATED";
    }

    @Override
    public List<PlayerRecord> computeListOfPlayersAt(String positionString) {
        List<PlayerRecord> theList = new ArrayList<>();
        for (String id : playerId2PlayerSpecs.keySet()) {
            PlayerRecord player = playerId2PlayerSpecs.get(id);
            if (player.isInCave() && player.getPositionAsString().equals(positionString)) {
                theList.add(player);
            }
        }
        return theList;
    }

    @Override
    public String CheckState() {
        return "Fake";
    }
}
