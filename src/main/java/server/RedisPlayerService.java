package server;

import com.google.gson.Gson;
import domain.PlayerRecord;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import service.PlayerService;

import java.util.ArrayList;
import java.util.List;

public class RedisPlayerService implements PlayerService {

    private JedisPool pool;
    private Gson gson;
    private String address;
    private int port;

    public RedisPlayerService(){
        gson = new Gson();
    }

    public RedisPlayerService(String address, int port){
        this();
        this.address = address;
        this.port = port;
        pool = new JedisPool(address, port);
    }

    @Override
    public PlayerRecord getPlayerById(String playerId) {
        try (Jedis jedis = pool.getResource()) {
            String playerJson = jedis.get(createPlayerKey(playerId));
            if (playerJson == null)
                return null;
            return gson.fromJson(playerJson, PlayerRecord.class);
        }
    }

    @Override
    public String updatePlayerRecord(PlayerRecord player) {
        String playerKey = createPlayerKey(player.getPlayerId());
        boolean update;
        String status;
        try (Jedis jedis = pool.getResource()) {
            update = jedis.exists(playerKey);
            status = jedis.set(playerKey, gson.toJson(player));
        }

        return update && status != null && status.equals("OK") ? "UPDATED"
                : status != null && status.equals("OK") ? "CREATED"
                : "ERROR";
    }

    @Override
    public List<PlayerRecord> computeListOfPlayersAt(String positionString) {
        try (Jedis jedis = pool.getResource()) {

            List<PlayerRecord> players = new ArrayList<>();
            for (String key :
                    jedis.keys("player#*")) {
                String playerJson = jedis.get(key);
                PlayerRecord player = gson.fromJson(playerJson, PlayerRecord.class);
                if (player.getPositionAsString().equals(positionString))
                    players.add(player);
            }
            return players;
        }
    }

    @Override
    public String CheckState() {
        return "Redis:"+ address + ":"+ port;
    }

    private static String createPlayerKey(String playerId) {
        return "player#" + playerId;
    }
}
