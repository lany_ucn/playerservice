package service;

import domain.PlayerRecord;

import java.util.List;

public interface PlayerService {

    // getPlayerById
    PlayerRecord getPlayerById(String playerId);

    // updatePlayerRecord
    String updatePlayerRecord(PlayerRecord player);

    // computeListOfPlayersAt
    List<PlayerRecord> computeListOfPlayersAt(String positionString);

    String CheckState();
}
