FROM henrikbaerbak/jdk11-gradle68

LABEL author="Lars Nysom (lany@ucn.dk)"
LABEL description="Image for the PlayerService from Iteration 2.1"

WORKDIR root/playerservice

COPY . .

EXPOSE 39021

CMD gradle playerservice